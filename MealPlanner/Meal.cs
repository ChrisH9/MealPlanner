﻿namespace MealPlanner
{
    public class Meal
    {
        public string Date { get; set; }

        public string MenuItem { get; set; }
    }
}