﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using ExcelDataReader;

namespace MealPlanner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly List<string> menuItems;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            Meals = new ObservableCollection<Meal>();            

            menuItems = GetMenuItems();
            int totalMenuItemChoices = menuItems.Count;            
            DaysToPlan = new ObservableCollection<int>(Enumerable.Range(1, totalMenuItemChoices));
            SelectedDaysToPlan = totalMenuItemChoices >= 7 ? 7 : totalMenuItemChoices;

            StartingDate = DateTime.Today;
        }

        public ObservableCollection<Meal> Meals { get; set; }

        public ObservableCollection<int> DaysToPlan { get; set; }

        public int SelectedDaysToPlan { get; set; }

        public DateTime StartingDate { get; set; }

        private void OnClickCreateMenu(object sender, RoutedEventArgs e)
        {
            try
            {
                Meals.Clear();

                menuItems.Shuffle();

                for (int i = 0; i < SelectedDaysToPlan; i++)
                {
                    Meals.Add(new Meal
                    {
                        Date = StartingDate.AddDays(i).ToShortDateString(),
                        MenuItem = menuItems[i]
                    });
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private List<string> GetMenuItems()
        {
            List<string> mealItems = new List<string>();

            try
            {
                string dir = AppDomain.CurrentDomain.BaseDirectory + "MenuItems.xlsx";

                using (var stream = File.Open(dir, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        do
                        {
                            while (reader.Read())
                            {
                                var meal = reader.GetString(0);
                                if (!string.IsNullOrWhiteSpace(meal))
                                {
                                    mealItems.Add(meal);
                                }
                            }
                        } while (reader.NextResult());
                    }
                }

                return mealItems;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                return null;
            }
        }

        private void HandleException(Exception ex)
        {
            MessageBox.Show($"An error occured.{Environment.NewLine + ex.Message}");
            Close();
        }
    }
}
